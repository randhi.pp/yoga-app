<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GetDataAndSend extends Controller
{
    function getData () {
        $response = Http::get('https://dummyjson.com/products');
        return $response->json();
    }

    function process($data) {
        // rubah data
        //$data->products[0]->title = "Laskar Pelangi";

        return $data;
    }

    public function sendData () {
        $data = $this->getData();
        $result = $this->process($data);
        
        // sudah melakukan curl
        $response = Http::post(
            'https://httpdump.app/inspect/fe2b1a4b-6229-417a-8a50-a7aa04bfb93c',
            $result
        );
        Log::info($response->json());
    }
}
